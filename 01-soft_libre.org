#+BEGIN_EXPORT beamer
\setemojifont{NotoColorEmoji.ttf}[Path=./]

\AtBeginSection[]{
  \begin{frame}{Outline}
    \tableofcontents[currentsection, hideallsubsections]
  \end{frame}
}

\AtBeginSubsection[]{
  \begin{frame}{Outline}
    \tableofcontents[sectionstyle=show/hide, 
    subsectionstyle=show/shaded/hide]
  \end{frame}
}

\beamerdefaultoverlayspecification{<+->}

% \setsansfont{SegoeUIEmoji.ttf}[
%   RawFeature={+colr;+dist;+ccmp},
%   BoldFont={Segoe UI Bold},
%   ItalicFont={Segoe UI Italic},
%   BoldItalicFont={Segoe UI Bold Italic}]
%% \newcommand{\emoji}[1]{{\setsansfont{Noto Color Emoji}[Renderer=Harfbuzz]{}#1}}
#+END_EXPORT

* Software Libre
** ¿Qué es Software Libre?
*** El respeto al usuario
**** Objetivo principal                                     :B_alertblock:
:PROPERTIES:
:BEAMER_env: alertblock
:END:
El respeto a las libertades del usuario.

***** Nota 1                                                     :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:
El objetivo principal del Software Libre es el respeto a las libertades del usuario. 

Pero, ¿quiénes son los usuarios de un software?


**** \busts ¿Quiénes son usuarios?

- \nerdface Usan el programa
- \openhands Lo tienen y comparten el programa
- \technologist Desarrolladores
- \graduationcap Académicos, \microscope científicos, \gear ingenieros, etc.

***** Nota 2                                                     :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:

Los usuarios pueden ser aquellos que lo usan (valga la redundancia), los que solo lo tienen guardado y lo quieren copiar y compartir. Los desarrolladores también son usuarios: de bibliotecas, APIs, etc.


**** \earthglobe Definición en la Web:
\onslide+<+->
#+attr_latex: :height 1cm :center nil :float nil
[[./imgs/800px-Heckert_GNU_white.svg.png]] https://www.gnu.org/philosophy/free-sw.html

*** Las cuatro libertades
Entonces, dependiendo del tipo de usuario: ¿qué se debe respetar?

**** 1                                                            :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:
Basado en los tipos de usuarios mencionados, ¿qué clase de libertades deben respetarse?


**** \nerdface Usuarios que usan el programa
Ejecutar el software.

**** 2                                                            :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:

Si son usuarios que simplemente utilizan el software, necesitan poder ejecutarlo. Por eso, ejecutar el programa es parte de una libertad que debe ser respetada. 

**** \openhands Usuarios que tienen y comparten el programa
Copiar y distribuir el software.

**** \technologist Usuarios que son desarrolladores
Estudiar, cambiar y mejorar el software.

*** Comparar con software privativo
Comparemos las libertades con el software privativo:

**** 1                                                            :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:
Pensemos desde el punto de vista contrario, y también desde nuestra experiencia cotidiana. ¿Qué nos ha sucedido con el software? ¿cómo infringe las libertades planteadas?

Mientras, aprovechamos a describir las cuatro libertades propuestas por la FSF como definición del Software Libre.
 
**** Libertad 0: \play Ejecutar

- *Trialware*
- *Crippleware*
- *Nag/beg/annoyware*

**** Libertad 1: \glassright Estudiar y hacer cambios

- Licencias privativas
  # EULA (¿Algún sistema operativo conocido?)
- +Ingeniería inversa+
- ¿Y los sitios Webs en PHP/ASP/etc.?

*** Comparar con software privativo

**** Libertad 2: \openhands Distribuir copias

- Shareware (los famosos Demos de software ¿con qué abrían los zips?)
- Videojuegos (disquettes, cartuchos, CD, DVD, BlueRays, Steam)

**** Libertad 3: \recycle Distribuir copias con modificaciones
Mismos que el anterior... 

****                                                           :B_columns:
:PROPERTIES:
:BEAMER_env: columns
:END:
***** Copyleft                                            :B_block:BMCOL:
:PROPERTIES:
:BEAMER_col: .7
:BEAMER_env: block
:END:
Libertad 2 y 3 mencionan Copyleft.
#+begin_quote
``(very simply stated) is the rule that when redistributing the program, you cannot add restrictions to deny other people the central freedoms.''
#+end_quote
 https://www.gnu.org/copyleft/
*****                                                             :BMCOL:
:PROPERTIES:
:BEAMER_col: .2
:END:
\onslide+<+->
#+attr_latex: :height 2cm :float nil
[[./imgs/copyleft.png]]
*** Freeware vs. Free Software

**** ¿FreeWare?
Software que es distribuido sin costo monetario. 

**** Pero, el Free Software... ¿Es gratuito?
\onslide+<+->
#+attr_latex: :width 7cm :float nil
[[./imgs/fsf.png]]

- ¿Puedo cobrar por el Software Libre?
  - Por hacerlo
  - Por distribuirlo
  - Por arreglarlo, modificarlo, etc.
- La FSF propone una alternativa al nombre: ``Free'' \rarr ``Libre''

** Formatos libres
*** Una aclaración sobre los formatos
**** ¿Los formatos de archivo pueden ser SL?

- ¿Qué pasa con los archivos de datos?
- ¿También pueden clasificarse entre Software Libre/Privativo?

**** MP3, RAR, DOC, XLS, etc.

- 2017 la /patente/ de MP3 fue terminada.
  - https://www.fsf.org/campaigns/playogg
  - https://www.gnu.org/philosophy/fighting-software-patents.html
- ¿Qué pasa con RAR?
- ¿Qué pasa con los documentos y hojas de cálculo?  
- Foundation for a Free Information Infraestructure https://ffii.org/
  - Hacer software requiere de muchos componentes
  - ¿Estarán patentados?

**** Nota acerca de Patentes                                      :B_note:
:PROPERTIES:
:BEAMER_env: note
:END:
En Estados Unidos, antes del 2017, la FSF realizó una campaña llamada Play OGG con la intención de poner en evidencia que el MP3 estaba patentado en ese país. Los archivos en ese formato y las bibliotecas que pueden leerlos y escribirlos podían ser utilizado si se adquiría la licencia o el permiso correspondiente de la entidad que lo patentó.

Para evitar que la empresa o persona que patentó el software tenga control sobre el formato y su uso, la FSF propone utilizar archivos OGG en vez de MP3. La campaña promueve la utilización de este formato de estándar abierto y con implementaciones libres.

La FFII aboga por mantener el software libre de patentes. Busca mostrar el efecto de las patentes existentes en componentes de software sobre los nuevos desarrollos.

**** En Argentina:                                              :noexport:
El software no se patenta. Pero se respeta la decisión del autor.

*** Digital Restriction Management (DRM)

#+latex: \begin{center}
#+attr_latex: :height 2cm :center nil
[[./imgs/508px-Button-dbd.svg.png]]
#+attr_latex: :height 2cm :center nil 
[[./imgs/DRM-free_label.en.png]]
#+latex: \end{center}

**** ¿DRM?
Digital Restriction Management (DRM) es la práctica de *imponer restricciones* que controlan lo que el usuario puede hacer con un software o dispositivo. 
https://defectivebydesign.org/

**** Ejemplos:

- Digital Books: ¿Se puede leer un libro digital en cualquier lado?
- Canciones: ¿Puedo copiar esta canción que compré para escucharla en el auto?
- ¿Puedo jugar un videojuego sin tener que conectarme a Internet?

*** ¿Cuál es el problema si no se respeta al usuario?

**** ¿Cómo se carga o guarda un archivo?
  - No se sabe y no hay código para estudiar.
  - +Hacer Ingeniería Inversa+ \larr ¡Prohibido por licencias!
  - Puede no haber bibliotecas o API disponible.

****  ¿Se puede usar el archivo?
  - Depende de la licencia/patente.

****  ¿Puedo cambiar algo dentro del archivo?
  - No puedo estudiar el formato.
  - No hay estándar alguno.
  - ¿se imaginan algún caso típico con .doc y .xls?

*** ¿Y en la vida real? ¿Cuál es el problema?

**** Entidades privadas
Empresa que no pagó la licencia a tiempo...
- ¿Qué pasa con sus documentos?
- ¿Qué sucede con su trabajo?
  - Contaduría, secretarías, RRHH...

**** Entidades públicas
Entidades públicas que usa formatos privativos:

- Ej. Municipalidades, Universidades, Poder Judicial...
- ¿Sus usuarios deberían comprar las licencias para ver un documento?
- ¡Sus usuarios también somos nosotros!
  - ¿Cómo hacemos para leer un instructivo, expediente, enunciado de TP...?

*** ¿Y en la vida real? ¿Cuál es el problema? (Cont.)

**** Desarrolladores y Administradores de Computadoras
¿Qué sucede con los desarrolladores y administradores?

- ¿Cómo puedo desarrollar un programa/script si no conozco el formato?
  - Se complica automatizar tareas.
- ¿Y si quisiera transformarlo a otro formato para manipularlo?
- ¿Cómo afecta ante la migración de sistemas, el cambio de interfaces, etc.?


* Software Libre en Instituciones Públicas

** Un poco de historia                                            :noexport:

*** Etapa Pre-informática
Supongamos una biblioteca pública. ¿Cómo gestionaban los libros?

\tiny 
Toda similitud con la realidad es mera coincidencia... ;)
\normalsize


**** Archivos

- Un archivo de tarjetas.
- Mantenimiento constante.
  - Órden de las fichas
  - Datos
- Búsqueda manual.
- Personal de mantenimiento.

**** ¡Quiero un libro! Pero... No me acuerdo el título...   :B_alertblock:
Le preguntamos a la persona que está en mesa de entrada...

Encontrar el libro depende de su conocimiento
# y buena voluntad.

*** Comienzos de la informatización
Luego, el archivo de fichas se digitalizó...

**** Digitalización del archivo

- Aún requería mantenimiento constante:
  - Data entry.
- Búsqueda digital.
- Personal de mantenimiento enfocado en tareas de la institución.

**** Software de archivo

- Típicamente era Software Privativo.
- Al ser especializado, no hay software enlatado.
  - Se contrataba una persona para hacerlo.
- Mantenimiento del software.
  - Dependencia con el autor.
  - Posiblemente respuesta tardía.

*** Comienzos de la informatización
Luego, el archivo de fichas se digitalizó...

**** Digitalización del archivo
- Aún requería mantenimiento constante:
  - Data entry.
- Búsqueda automática.
- Personal de mantenimiento enfocado en tareas de la institución.

**** ¿Qué pasa ahora si quiero un libro?                    :B_alertblock:
:PROPERTIES:
:BEAMER_env: alertblock
:END:
No dependemos del conocimiento del personal \to ¡Toda la información a nuestro alcance!

¡Podemos buscarlo nosotros mismos!

*** Si todo funciona, ¿por qué Software Libre?

**** ¿Qué pasa con el archivo?

- Actualizaciones de software.
- Migraciones a nuevos formatos.
- ¿Se podrá Publicar a Internet?
- Agregar nuevas características.

**** ¿Qué puede pasarle al personal?

- Encuentran problemas en el software.
- No pueden arreglarlo.
- No pueden compartirlo a sus compañeros.
- No pueden utilizarlo en sus oficinas o casas.

*** Si todo funciona, ¿por qué Software Libre?

**** ¿Qué pasa con la institución?

- Se estanca tecnológicamente.
- Cada vez cuesta más migrar a nuevas tecnologías: el archivo crece.
- Se genera una forma de trabajo acostumbrada a los problemas del software.
- Se acostumbran al proceso establecido por el software descartando posibles mejoras.

*** ¿Se imaginan algo similar en otros contextos?


- Poder Judicial.
- Municipalidad.
- Entes reguladores.
- Secretarías y ministerios.
- Escuelas primarias, secundarias, etc.

**** ¿Qué otros problemas pueden suceder?

- ¿Podrán acceder a la compra del software? Tanto la institución como los usuarios y el público.
- ¿Cuántas instituciones necesitan del software?
- ¿Se puede compartir información entre instituciones?
- ¿La información se podrá publicar fácilmente?
  - ¿Podrá el ciudadano utilizar los datos públicos para investigar?
- ¿Qué sucede con la transparencia?
  - ¿Hay un error de software o humano? 
  - ¿Podrá el ciudadano revisar si funciona todo bien?

*** ¿Se imaginan algo similar en otros contextos?
**** ¿Qué otros problemas pueden suceder?
- ¿Podrán acceder a la compra del software? Tanto la institución como los usuarios y el público.
- ¿Cuántas instituciones necesitan del software?
- ¿Se puede compartir información entre instituciones?
- ¿La información se podrá publicar fácilmente?
  - ¿Podrá el ciudadano utilizar los datos públicos para investigar?
- ¿Qué sucede con la transparencia?
  - ¿Hay un error de software o humano? 
  - ¿Podrá el ciudadano revisar si funciona todo bien?

**** ¿Gratuito o Libre?                                     :B_alertblock:
:PROPERTIES:
:BEAMER_env: alertblock
:END:
¿Importa que el software y los formatos sean gratuitos?

*¿O también deben respetar las libertades del usuario?*

** En la Universidad

*** ¿Y en la Universidad?
#+latex: \begin{center}
#+attr_latex: :height 2.5cm :center nil
[[./imgs/uncoma.png]]
#+attr_latex: :height 2.5cm :center nil
[[./imgs/unco-cartel.jpg]]
#+latex: \end{center}

¡Es una institución pública!
#+BEAMER: \pause

La universidad también tiene relación con la sociedad.

**** Enfocados al contexto social:

- Las investigaciones.
- La educación y capacitación.
- Certificación.
- Servicios, obras culturales y productos.

*** La Universidad y el FLOSS:
 Tratemos de relacionar lo que vimos con la Universidad...

**** Gestión interna
Necesidad de:

- Secretarías: compartir documentación.
- Software de gestión: Digestos, archivos, etc.
  - https://wene.fi.uncoma.edu.ar
  - https://siufai.uncoma.edu.ar (SIU Guaraní)
  - https://ranquel.uncoma.edu.ar
  - https://pedco.uncoma.edu.ar
- Secretaría de extensión: proyectos de extensión.
  - http://hornero.fi.uncoma.edu.ar/
- Secretaría de investigación: proyectos de investigación.
  - http://crowd.fi.uncoma.edu.ar/
- Otros proyectos de cursos de grado/posgrado.
  - https://juntar.fi.uncoma.edu.ar/

*** En la docencia y estudiantes

**** Materiales, recursos, documentos, datos... 
¿Cómo afectaría el SL al ...?

- Al acceso al software, la información y recursos materiales.
- A los recursos materiales: deben ser compartidos y actualizados.
- A las clases grabadas para estudiantes.
  - Por ejemplo: MIT OpenCourseWare: https://www.youtube.com/user/MIT
- A los trabajos prácticos de cursos que generan un producto.
- ¿Y a las tesis? Muchas de ellas generan prototipos y software.
- A los proyectos de becarios, que también generan documentación, datos y software.

*** En la investigación

**** Métodos de investigación
Experimentos, encuestas y entrevistas son métodos básicos en la investigación.

#+BEAMER: \pause
¡Y todos ellos generan gran cantidad de documentación y datos!

**** Ejemplos:

- Comparación de tecnologías y sus mejoras.
  - Muchas veces se hace necesario probar herramientas.
- Benchmarking.
  - Acceso al generador de benchmarks.
  - Acceso a la herramienta de prueba.
- Construir nuestros prototipos.
  - Hace falta acceso a las bibliotecas de software.

**** Revisión de Pares
¿Cómo podríamos probar nuestros prototipos sin revisión ni testeos?


* Licencia
** Licencia de Esta Obra
*** Licencia de Esta obra

\footnotesize
Excepto en los lugares que se ha indicado lo contrario:

\centering
Este obra está bajo una licencia de Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional.

#+LATEX: \ \\

#+latex: \begin{center}
#+attr_latex: :width 2cm :center nil :float nil
[[./imgs/cc-cc.png]]
#+attr_latex: :width 2cm :center nil :float nil
[[./imgs/cc-by.png]]
#+attr_latex: :width 2cm :center nil :float nil
[[./imgs/cc-sa.png]]
#+latex: \end{center}
  
**** CC-By-SA                                                    :B_block:
:PROPERTIES:
:BEAMER_env: block
:END:
\footnotesize
Excepto en los lugares que se ha indicado lo contrario:

Este obra está bajo una licencia de Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional. Para ver una copia de esta licencia, visite [[http://creativecommons.org/licenses/by-sa/4.0/]].


** Imágenes

*** Licencia de las imágenes

\scriptsize
#+attr_latex: :height 1cm :float nil :center nil
file:imgs/800px-Heckert_GNU_white.svg.png  ``A Bold GNU Goat Head'' por Aurelio A. Heckert bajo la licencia Free Art License y Creative Commons Attribution-ShareAlike 2.0. https://en.wikipedia.org/wiki/File:Heckert_GNU_white.svg

#+attr_latex: :height 1cm :float nil :center nil
file:imgs/copyleft.png ``Logotipo del símbolo Copyleft, consistente en un un símbolo de Copyright con la C dada la vuelta.'' por Zscout370, Sertion, e.a. y se encuentra bajo el dominio público. https://commons.wikimedia.org/wiki/File:Copyleft.svg

#+attr_latex: :height 1cm :float nil :center nil
file:imgs/fsf.png Logo de la Free Software Foundation por la Free Software Foundation, Inc. bajo la licencia Creative Commons Attribution-No Derivative Works 3.0 license (or later version). https://www.fsf.org/

#+attr_latex: :height 1cm :float nil :center nil
file:imgs/508px-Button-dbd.svg.png  Logo de Defective by Design por la Free Software Foundation, Inc. bajo la licencia Creative Commons Attribution-ShareAlike 3.0. https://www.defectivebydesign.org/printable

*** Licencia de las imágenes

\scriptsize
#+attr_latex: :height 1cm :float nil :center nil
file:imgs/DRM-free_label.en.png  Etiqueta ``DRM-free'' por la Free Software Foundation, Inc. bajo la licencia  Creative Commons Attribution-ShareAlike 3.0 Unported License. https://www.defectivebydesign.org/drm-free/how-to-use-label

#+attr_latex: :height 1cm :float nil :center nil
[[file:imgs/uncoma.png]]
Obtenido desde el Diario Río Negro.
https://www.rionegro.com.ar/los-casos-de-coronavirus-vinculados-a-la-universidad-son-de-neuquen-y-bariloche-1464325/

#+attr_latex: :height 1cm :float nil :center nil
[[file:imgs/unco-cartel.jpg]] Obtenido desde La Mañana de Cipolletti. https://www.lmcipolletti.com/el-ciclo-lectivo-2020-la-unco-sera-virtual-n711705

* Meta     :noexport:

# ----------------------------------------------------------------------
#+TITLE:  Introducción al Software Libre y Open Source
#+AUTHOR: Christian Gimenez
# #+DATE: 24 de Abril del 2021
# #+DATE:   18 de Noviembre del 2020
#+EMAIL:
#+DESCRIPTION: 
#+KEYWORDS: 

#+STARTUP: inlineimages hidestars content hideblocks entitiespretty
#+STARTUP: indent fninline latexpreview

#+OPTIONS: H:3 num:t toc:t \n:nil @:t ::t |:t ^:{} -:t f:t *:t <:t
#+OPTIONS: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS: tex:imagemagick

#+TODO: TODO(t!) CURRENT(c!) PAUSED(p!) | DONE(d!) CANCELED(C!@)

# -- Export
#+LANGUAGE: es
#+LINK_UP:   
#+LINK_HOME: 
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport

# -- HTML Export
#+INFOJS_OPT: view:info toc:t ftoc:t ltoc:t mouse:underline buttons:t path:libs/org-info.js
#+HTML_LINK_UP: index.html
#+HTML_LINK_HOME: index.html
#+XSLT:

# -- For ox-twbs or HTML Export
# #+HTML_HEAD: <link href="libs/bootstrap.min.css" rel="stylesheet">
# -- -- LaTeX-CSS
# #+HTML_HEAD: <link href="css/style-org.css" rel="stylesheet">

# #+HTML_HEAD: <script src="libs/jquery.min.js"></script> 
# #+HTML_HEAD: <script src="libs/bootstrap.min.js"></script>


# -- LaTeX Export
#+latex_compiler: lualatex
#+latex_class: beamer
#+beamer_theme: Madrid
# #+latex_class_options: [presentation]
#+latex_class_options: [handout]
#+LATEX_HEADER: \logo{\includegraphics[height=0.25cm]{imgs/cc-by-sa-small.png}}
#+LATEX_HEADER: \setbeamercovered{transparent}
# #+LATEX_HEADER: \setbeameroption{show notes on second screen}

#+latex_header: \usepackage{fontspec}
#+latex_header: \usepackage{emoji}

# -- -- Tikz
# #+LATEX_HEADER: \usepackage{tikz}
# #+LATEX_HEADER: \usetikzlibrary{shapes.geometric}
# #+LATEX_HEADER: \usetikzlibrary{shapes.symbols}
# #+LATEX_HEADER: \usetikzlibrary{positioning}
# #+LATEX_HEADER: \usetikzlibrary{trees}

# #+LATEX_HEADER_EXTRA:

# Local Variables:
# org-hide-emphasis-markers: t
# org-use-sub-superscripts: "{}"
# fill-column: 80
# visual-line-fringe-indicators: t
# ispell-local-dictionary: "spanish"
# End:

